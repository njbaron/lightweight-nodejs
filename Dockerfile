# Build
#   docker build -t nodeapp .
# Run
#   docker run --name nodeapp -p 9999:9999 nodeapp
# Stop
#   docker stop nodeapp && docker rm nodeapp

FROM node:14

WORKDIR /app

COPY app/ .

RUN npm ci

EXPOSE 9999

CMD [ "node", "index.js" ]